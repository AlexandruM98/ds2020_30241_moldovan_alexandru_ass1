package ro.tuc.ds2020.entities;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "medicationPlan")
public class MedicationPlan {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Type(type = "uuid-binary")
    private UUID id;

    @OneToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
    List<IntakeIntervals> medicationsAndIntakeIntervals;

    @Column(name = "period", nullable = false)
    private String period;

    public MedicationPlan() {

    }

    public MedicationPlan(UUID id, List<IntakeIntervals> medicationsAndIntakeIntervals, String period) {
        this.id = id;
        this.medicationsAndIntakeIntervals = medicationsAndIntakeIntervals;
        this.period = period;
    }

    public MedicationPlan(List<IntakeIntervals> medicationsAndIntakeIntervals, String period) {
        this.medicationsAndIntakeIntervals = medicationsAndIntakeIntervals;
        this.period = period;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public List<IntakeIntervals> getMedicationsAndIntakeIntervals() {
        return medicationsAndIntakeIntervals;
    }

    public void setMedicationsAndIntakeIntervals(List<IntakeIntervals> medicationsAndIntakeIntervals) {
        this.medicationsAndIntakeIntervals = medicationsAndIntakeIntervals;
    }
}
