package ro.tuc.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class PatientDTO extends PersonDTO {
    @NotNull
    private String medicalRecord;

    public PatientDTO() {
    }

    public PatientDTO(String name, String address, int age, String gender, String username, String password, String medicalRecord) {
        super(name, address, age, gender, username, password);
        this.medicalRecord = medicalRecord;
    }

    public PatientDTO(UUID id, String name, String address, int age, String gender, String username, String password, String medicalRecord) {
        super(id, name, address, age, gender, username, password);
        this.medicalRecord = medicalRecord;
    }

    public String getMedicalRecord() {
        return medicalRecord;
    }

    public void setMedicalRecord(String medicalRecord) {
        this.medicalRecord = medicalRecord;
    }

}
