package ro.tuc.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

public class CaregiverDTO extends PersonDTO {
    @NotNull
    private List<PatientDTO> patients;

    public CaregiverDTO() {
    }

    public CaregiverDTO(String name, String address, int age, String gender, String username, String password, List<PatientDTO> patients) {
        super(name, address, age, gender, username, password);
        this.patients = patients;
    }

    public CaregiverDTO(UUID id, String name, String address, int age, String gender, String username, String password, List<PatientDTO> patients) {
        super(id, name, address, age, gender, username, password);
        this.patients = patients;
    }

    public List<PatientDTO> getPatients() {
        return patients;
    }

    public void setPatients(List<PatientDTO> patients) {
        this.patients = patients;
    }
}
