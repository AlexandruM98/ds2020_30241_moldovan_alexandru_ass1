package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.Patient;

public class PatientBuilder {
    private PatientBuilder() {
    }

    public static PatientDTO toDto(Patient patient) {
        return new PatientDTO(
                patient.getId(),
                patient.getName(),
                patient.getAddress(),
                patient.getAge(),
                patient.getGender(),
                patient.getUsername(),
                patient.getPassword(),
                patient.getMedicalRecord()
        );
    }

    public static Patient toEntity(PatientDTO patientDTO) {
        return new Patient(
                patientDTO.getId(),
                patientDTO.getName(),
                patientDTO.getAddress(),
                patientDTO.getAge(),
                patientDTO.getGender(),
                patientDTO.getUsername(),
                patientDTO.getPassword(),
                patientDTO.getMedicalRecord()
        );
    }
}
