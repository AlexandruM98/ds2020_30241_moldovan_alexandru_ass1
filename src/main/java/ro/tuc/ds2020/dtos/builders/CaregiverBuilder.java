package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Patient;

import java.util.List;
import java.util.stream.Collectors;

public class CaregiverBuilder {
    public static CaregiverDTO toDto(Caregiver caregiver) {
        List<PatientDTO> patientsDTO = caregiver.getPatients().stream()
                .map(PatientBuilder::toDto)
                .collect(Collectors.toList());
        return new CaregiverDTO(
                caregiver.getId(),
                caregiver.getName(),
                caregiver.getAddress(),
                caregiver.getAge(),
                caregiver.getGender(),
                caregiver.getUsername(),
                caregiver.getPassword(),
                patientsDTO
        );
    }

    public static Caregiver toEntity(CaregiverDTO caregiverDTO) {
        List<Patient> patients = caregiverDTO.getPatients().stream()
                .map(PatientBuilder::toEntity)
                .collect(Collectors.toList());
        return new Caregiver(
                caregiverDTO.getId(),
                caregiverDTO.getName(),
                caregiverDTO.getAddress(),
                caregiverDTO.getAge(),
                caregiverDTO.getGender(),
                caregiverDTO.getUsername(),
                caregiverDTO.getPassword(),
                patients
        );
    }
}
