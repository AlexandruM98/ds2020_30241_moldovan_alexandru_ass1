package ro.tuc.ds2020.dtos;

import ro.tuc.ds2020.entities.Medication;

import javax.validation.constraints.NotNull;
import java.util.UUID;

public class IntakeIntervalsDTO {

    private UUID id;

    @NotNull
    private Medication medication;

    @NotNull
    private boolean morning;

    @NotNull
    private boolean noon;

    @NotNull
    private boolean evening;

    public IntakeIntervalsDTO() {

    }

    public IntakeIntervalsDTO(Medication medication, boolean morning, boolean noon, boolean evening) {
        this.medication = medication;
        this.morning = morning;
        this.noon = noon;
        this.evening = evening;
    }


    public IntakeIntervalsDTO(UUID id, Medication medication, boolean morning, boolean noon, boolean evening) {
        this.id = id;
        this.medication = medication;
        this.morning = morning;
        this.noon = noon;
        this.evening = evening;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Medication getMedication() {
        return medication;
    }

    public void setMedication(Medication medication) {
        this.medication = medication;
    }

    public boolean isMorning() {
        return morning;
    }

    public void setMorning(boolean morning) {
        this.morning = morning;
    }

    public boolean isNoon() {
        return noon;
    }

    public void setNoon(boolean noon) {
        this.noon = noon;
    }

    public boolean isEvening() {
        return evening;
    }

    public void setEvening(boolean evening) {
        this.evening = evening;
    }
}
