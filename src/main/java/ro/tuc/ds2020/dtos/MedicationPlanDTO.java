package ro.tuc.ds2020.dtos;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

public class MedicationPlanDTO {

    private UUID id;

    @NotNull
    private List<IntakeIntervalsDTO> medicationsAndIntakeIntervals;

    @NotNull
    private String period;

    public MedicationPlanDTO() {

    }

    public MedicationPlanDTO(UUID id, List<IntakeIntervalsDTO> medicationsAndIntakeIntervalsDTO, String period) {
        this.id = id;
        this.medicationsAndIntakeIntervals = medicationsAndIntakeIntervalsDTO;
        this.period = period;
    }

    public MedicationPlanDTO(List<IntakeIntervalsDTO> medicationsAndIntakeIntervalsDTO, String period) {
        this.medicationsAndIntakeIntervals = medicationsAndIntakeIntervalsDTO;
        this.period = period;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public List<IntakeIntervalsDTO> getMedicationsAndIntakeIntervals() {
        return medicationsAndIntakeIntervals;
    }

    public void setMedicationsAndIntakeIntervals(List<IntakeIntervalsDTO> medicationsAndIntakeIntervals) {
        this.medicationsAndIntakeIntervals = medicationsAndIntakeIntervals;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }
}
