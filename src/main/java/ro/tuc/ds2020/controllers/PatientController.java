package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.services.PatientService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/patient")
public class PatientController {

    private final PatientService patientService;

    @Autowired
    public PatientController(PatientService patientService) {
        this.patientService = patientService;
    }

    @GetMapping()
    public Page<PatientDTO> getPatients(@PageableDefault(Integer.MAX_VALUE) Pageable pageable) {
        return patientService.findPatients(pageable).map(PatientBuilder::toDto);
    }

    @PostMapping()
    public PatientDTO insertPatient(@Valid @RequestBody PatientDTO patientDTO) {
        Patient patient = patientService.insert(PatientBuilder.toEntity(patientDTO));
        return PatientBuilder.toDto(patient);
    }

    @GetMapping(value = "/{id}")
    public PatientDTO getPatient(@PathVariable("id") UUID patientID) {
        return PatientBuilder.toDto(patientService.findPatientById(patientID));
    }

    @DeleteMapping(value = "/{id}")
    public void deletePatient(@PathVariable("id") UUID id) {
        this.patientService.delete(id);
    }

    @PutMapping
    public PatientDTO updatePatient(@RequestBody PatientDTO patientDTO) {
        Patient updatedPatient = patientService.update(PatientBuilder.toEntity(patientDTO));
        return PatientBuilder.toDto(updatedPatient);
    }

}
