package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.PersonDTO;
import ro.tuc.ds2020.dtos.builders.PersonBuilder;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.services.PersonService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/person")
public class PersonController {

    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping()
    public Page<PersonDTO> getPersons(@PageableDefault(Integer.MAX_VALUE) Pageable pageable) {
        return personService.findPersons(pageable).map(PersonBuilder::toDto);
    }

    @PostMapping()
    public PersonDTO insertPerson(@Valid @RequestBody PersonDTO personDTO) {
        Person person = personService.insert(PersonBuilder.toEntity(personDTO));
        return PersonBuilder.toDto(person);
    }

    @GetMapping(value = "/{id}")
    public PersonDTO getPerson(@PathVariable("id") UUID personId) {
        return PersonBuilder.toDto(personService.findPersonById(personId));
    }

    @DeleteMapping(value = "/{id}")
    public void deletePerson(@PathVariable("id") UUID id) {
        this.personService.delete(id);
    }

    @PutMapping
    public PersonDTO updatePerson(@RequestBody PersonDTO personDTO) {
        Person updatedPerson = personService.update(PersonBuilder.toEntity(personDTO));
        return PersonBuilder.toDto(updatedPerson);
    }


}
