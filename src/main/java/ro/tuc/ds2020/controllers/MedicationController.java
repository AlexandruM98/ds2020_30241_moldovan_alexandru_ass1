package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.dtos.builders.MedicationBuilder;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.services.MedicationService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication")
public class MedicationController {
    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping()
    public Page<MedicationDTO> getMedications(@PageableDefault(Integer.MAX_VALUE) Pageable pageable) {
        return medicationService.findMedications(pageable).map(MedicationBuilder::toDto);
    }

    @PostMapping()
    public MedicationDTO insertMedication(@Valid @RequestBody MedicationDTO medicationDTO) {
        Medication medication = medicationService.insert(MedicationBuilder.toEntity(medicationDTO));
        return MedicationBuilder.toDto(medication);
    }

    @GetMapping(value = "/{id}")
    public MedicationDTO getMedication(@PathVariable("id") UUID medicationId) {
        return MedicationBuilder.toDto(medicationService.findMedicationById(medicationId));
    }

    @DeleteMapping(value = "/{id}")
    public void deleteMedication(@PathVariable("id") UUID id) {
        this.medicationService.delete(id);
    }

    @PutMapping
    public MedicationDTO updateMedication(@RequestBody MedicationDTO medicationDTO) {
        Medication updatedMedication = medicationService.update(MedicationBuilder.toEntity(medicationDTO));
        return MedicationBuilder.toDto(updatedMedication);
    }

}
