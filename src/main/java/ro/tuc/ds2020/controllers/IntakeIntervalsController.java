package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.IntakeIntervalsDTO;
import ro.tuc.ds2020.dtos.builders.IntakeIntervalsBuilder;
import ro.tuc.ds2020.entities.IntakeIntervals;
import ro.tuc.ds2020.services.IntakeIntervalsService;

import javax.validation.Valid;

@RestController
@CrossOrigin
@RequestMapping(value = "/intake-intervals")
public class IntakeIntervalsController {

    private final IntakeIntervalsService intakeIntervalsService;

    @Autowired
    public IntakeIntervalsController(IntakeIntervalsService intakeIntervalsService) {
        this.intakeIntervalsService = intakeIntervalsService;
    }

    @PostMapping()
    public IntakeIntervalsDTO insertIntakeIntervals(@Valid @RequestBody IntakeIntervalsDTO intakeIntervalsDTO) {
        IntakeIntervals intakeIntervals = intakeIntervalsService
                .insert(IntakeIntervalsBuilder.toEntity(intakeIntervalsDTO));
        return IntakeIntervalsBuilder.toDto(intakeIntervals);
    }

}
