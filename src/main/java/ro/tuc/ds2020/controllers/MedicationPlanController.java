package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.builders.MedicationPlanBuilder;
import ro.tuc.ds2020.entities.MedicationPlan;
import ro.tuc.ds2020.services.MedicationPlanService;

@RestController
@CrossOrigin
@RequestMapping(value = "/medication-plan")
public class MedicationPlanController {
    private final MedicationPlanService medicationPlanService;

    @Autowired
    public MedicationPlanController(MedicationPlanService medicationPlanService) {
        this.medicationPlanService = medicationPlanService;
    }

    @PostMapping()
    public MedicationPlanDTO insert(@RequestBody MedicationPlanDTO medicationPlanDTO) {
        MedicationPlan insertedMedPlan = medicationPlanService.insert(MedicationPlanBuilder.toEntity(medicationPlanDTO));
        return MedicationPlanBuilder.toDto(insertedMedPlan);
    }
}
