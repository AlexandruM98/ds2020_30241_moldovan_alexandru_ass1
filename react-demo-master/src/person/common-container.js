import React from 'react';
import APIResponseErrorMessage from "../commons/errorhandling/api-response-error-message";
import {Button, Card, CardHeader, Col, Modal, ModalBody, ModalHeader, Row} from 'reactstrap';
import PatientForm from "./components/forms/patient-form";
import CaregiverForm from "./components/forms/caregiver-form";
import MedicationForm from "./components/forms/medication-form";

import * as API_PATIENT from "./api/patient-api"
import * as API_CAREGIVER from "./api/caregiver-api"
import * as API_MEDICATION from "./api/medication-api"
import PatientTable from "./components/tables/patient-table";
import CaregiverTable from "./components/tables/caregiver-table";
import MedicationTable from "./components/tables/medication-table";


class CommonContainer extends React.Component {

    constructor(props) {
        super(props);
        this.toggleForm = this.toggleForm.bind(this);
        this.reload = this.reload.bind(this);
        this.state = {
            selected: false,
            collapseForm: false,
            tableData: [],
            isLoaded: false,
            errorStatus: 0,
            error: null
        };
        this.row = "";
        this.user = this.props.user;
        this.patients = [];
    }

    callbackFunction = (selected, row) => {
        this.setState({selected: selected})
        this.row = row;
    }

    componentDidMount() {
        this.loadData();
    }


    loadData() {
        if (this.user === "medication") {
            API_MEDICATION.getMedications((result, status, err) => {

                if (result !== null && status === 200) {
                    this.setState({
                        tableData: result.content,
                        isLoaded: true
                    });
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }));
                }
            });
        }
        if (this.user === "patient") {
            API_PATIENT.getPatients((result, status, err) => {

                if (result !== null && status === 200) {
                    this.setState({
                        tableData: result.content,
                        isLoaded: true
                    });
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }));
                }
            });
        }
        if (this.user === "caregiver") {
            API_CAREGIVER.getCaregivers((result, status, err) => {

                if (result !== null && status === 200) {
                    console.log(result.content);
                    this.setState({
                        tableData: result.content,
                        isLoaded: true
                    });
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }));
                }
            });
            API_PATIENT.getPatients((result, status, err) => {
                this.patients = [];
                if (result !== null && status === 200) {
                    for (let i = 0; i < result.content.length; i++) {
                        this.patients.push({
                            value: result.content[i].id,
                            label: result.content[i].name,
                            data: result.content[i]
                        });
                    }
                } else {
                    this.setState(({
                        errorStatus: status,
                        error: err
                    }));
                }
            });
        }
    }

    toggleForm() {
        this.setState({selected: !this.state.selected});
        this.row = "";
    }


    reload() {
        this.setState({
            isLoaded: false
        });
        this.toggleForm();
        this.loadData();
    }

    render() {
        return (
            <div>
                <CardHeader>
                    <strong> {this.user} Management </strong>
                </CardHeader>
                <Card>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            <Button color="primary" onClick={this.toggleForm}>Add {this.user} </Button>
                        </Col>
                    </Row>
                    <br/>
                    <Row>
                        <Col sm={{size: '8', offset: 1}}>
                            {(this.state.isLoaded && this.user === "patient") &&
                            <PatientTable tableData={this.state.tableData}
                                          parentCallBack={this.callbackFunction}
                                          reloadDataHandler={this.reload}/>}
                            {(this.state.isLoaded && this.user === "caregiver") &&
                            <CaregiverTable tableData={this.state.tableData}
                                            parentCallBack={this.callbackFunction}
                                            reloadDataHandler={this.reload}/>}
                            {(this.state.isLoaded && this.user === "medication") &&
                            <MedicationTable tableData={this.state.tableData}
                                             parentCallBack={this.callbackFunction}
                                             reloadDataHandler={this.reload}/>}
                            {this.state.errorStatus > 0 && <APIResponseErrorMessage
                                errorStatus={this.state.errorStatus}
                                error={this.state.error}
                            />}
                        </Col>
                    </Row>
                </Card>

                <Modal isOpen={this.state.selected} toggle={this.toggleForm}
                       className={this.props.className} size="lg">
                    <ModalHeader toggle={this.toggleForm}> Add {this.user}: </ModalHeader>
                    <ModalBody>
                        {this.user === "patient" && <PatientForm reloadHandler={this.reload}
                                                                 row={this.row}/>}
                        {this.user === "caregiver" && <CaregiverForm reloadHandler={this.reload}
                                                                     patients={this.patients}
                                                                     row={this.row}/>}
                        {this.user === "medication" && <MedicationForm reloadHandler={this.reload}
                                                                       row={this.row}/>}
                    </ModalBody>
                </Modal>

            </div>
        )

    }
}


export default CommonContainer;
