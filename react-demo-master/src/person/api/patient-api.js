import {HOST} from '../../commons/hosts';
import RestApiClient from "../../commons/api/rest-client";


const endpoint = {
    patient: '/patient/'
};

function getPatients(callback) {
    let request = new Request(HOST.backend_api + endpoint.patient, {
        method: 'GET',
    });
    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function getPatientById(params, callback) {
    let request = new Request(HOST.backend_api + endpoint.patient + params.id, {
        method: 'GET'
    });

    console.log(request.url);
    RestApiClient.performRequest(request, callback);
}

function savePatient(patient, callback) {
    let methodType = patient.id ? 'PUT' : 'POST';
    let request = new Request(HOST.backend_api + endpoint.patient, {
        method: methodType,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(patient)
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);
}

function deletePatient(patientId, callback) {
    let request = new Request(HOST.backend_api + endpoint.patient + patientId, {
        method: 'DELETE'
    });

    console.log("URL: " + request.url);

    RestApiClient.performRequest(request, callback);

}

export {
    getPatients,
    getPatientById,
    savePatient,
    deletePatient
};
