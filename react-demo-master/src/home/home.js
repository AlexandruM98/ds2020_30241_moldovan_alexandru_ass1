import React from 'react';

import BackgroundImg from '../commons/images/future-medicine.jpg';

import {Container, Jumbotron} from 'reactstrap';
import NavigationBarContainer from "../navigation-bar-container";

const backgroundStyle = {
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    width: "100%",
    height: "1920px",
    backgroundImage: `url(${BackgroundImg})`
};
const textStyle = {color: 'white',};

class Home extends React.Component {

    constructor(props) {
        super(props);
        this.role = this.props.role;
    }

    render() {
        const title = `data.${this.role}.title`;
        const description = `description.${this.role}.description`;
        return (
            <div>
                {
                    (this.role === "doctor" || this.role === "caregiver")
                    && <NavigationBarContainer user={this.role}/>
                }
                <Jumbotron fluid style={backgroundStyle}>
                    <Container fluid>
                        <h1 className="display-3" style={textStyle}>{title}</h1>
                        <p className="lead" style={textStyle}>
                            <b>{description}</b>
                        </p>
                    </Container>
                </Jumbotron>
            </div>
        )
    };
}

export default Home
