import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Home from './home/home';
import CommonContainer from './person/common-container'

import ErrorPage from './commons/errorhandling/error-page';
import styles from './commons/styles/project-style.css';

class App extends React.Component {


    render() {

        return (
            <div className={styles.back}>
                <Router>
                    <div>
                        <Switch>
                            <Route
                                exact
                                path='/'
                                render={() => <Home/>}
                            />

                            <Route
                                exact
                                path='/doctor'
                                render={() => <Home role={"doctor"}/>}
                            />

                            <Route
                                exact
                                path='/patient'
                                render={() => <Home role={"patient"}/>}
                            />

                            <Route
                                exact
                                path='/caregiver'
                                render={() => <Home role={"caregiver"}/>}
                            />

                            <Route
                                exact
                                path='/patient-management'
                                render={() => <CommonContainer user={"patient"}/>}
                            />

                            <Route
                                exact
                                path='/caregiver-management'
                                render={() => <CommonContainer user={"caregiver"}/>}
                            />

                            <Route
                                exact
                                path='/medication-management'
                                render={() => <CommonContainer user={"medication"}/>}
                            />
                            {/*Error*/}
                            <Route
                                exact
                                path='/error'
                                render={() => <ErrorPage/>}
                            />

                            <Route render={() => <ErrorPage/>}/>
                        </Switch>
                    </div>
                </Router>
            </div>
        )
    };
}

export default App
